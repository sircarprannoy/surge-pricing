package org.prannoy.spark

import org.apache.spark.SparkConf
import org.apache.spark.streaming.dstream.DStream
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.prannoy.util.Utility

object Streamer {
  Utility.setLogger()

  def main(args: Array[String]): Unit = {

    val conf = new SparkConf()
      .setMaster("local[2]")
      .setAppName("Test")
      .set("spark.driver.memory", "1g")

    val ssc = new StreamingContext(conf, Seconds(5))

    val driverPassengerDataStream = ssc.textFileStream("/Users/prannoy/kafka-consumer-write-to-file-path/")
    val weatherDataStream = ssc.textFileStream("/Users/prannoy/weather/")

    val demandSupplyStream: DStream[(String, Iterable[(String, Int)])] = driverPassengerDataStream.map(line => {
      val recordType = line.split(",")(0)
      val id = line.split(",")(1)
      val timestamp = line.split(",")(2)
      val latitude = line.split(",")(3)
      val longitude = line.split(",")(4)
      val geohash = line.split(",")(5)
      (Tuple2(geohash,recordType),1)
    }).reduceByKey((x,y) => x+y).map( x => {
      val geohash = x._1._1
      val recordType = x._1._2
      val count = x._2
      (geohash,Tuple2(recordType,count))
    }).groupByKey()

//    demandSupplyStream.print()


    val trafficStream: DStream[(String, Double)] = driverPassengerDataStream.map(line => {
      val recordType = line.split(",")(0)
      val id = line.split(",")(1)
      val timestamp = line.split(",")(2)
      val latitude = line.split(",")(3)
      val longitude = line.split(",")(4)
      val geohash = line.split(",")(5)
      ((recordType,geohash), Seq(Tuple3(latitude,longitude,timestamp)))
    }).reduceByKey((x,y) => x++y)
      .map(x => {
        val geohash = x._1._2
        val speed = getSpeed(x._2)
        Tuple2(geohash,speed)
      })
//      .print

    val weatherStream: DStream[(String, String)] = weatherDataStream.map(line => {
      val recordType = line.split(",")(0)
      val timestamp = line.split(",")(1)
      val latitude = line.split(",")(2)
      val longitude = line.split(",")(3)
      val geohash = line.split(",")(4)
      val weatherState = line.split(",")(15)
      (geohash, weatherState)
    })

    val joinedStreams = demandSupplyStream.join(trafficStream).join(weatherStream).print()



    ssc.start()
    ssc.awaitTermination()
  }

  def getSpeed(listArr : Seq[Tuple3[String, String, String]]): Double = {
    listArr.sortBy(x => x._3.toDouble)
    val startTimestamp = listArr.head._3.toDouble
    val endTimestamp = listArr.reverse.head._3.toDouble
    val startingPointLatitude = listArr.head._1.toDouble
    val startingPointLongitude = listArr.head._2.toDouble
    val endPointLatitude = listArr.reverse.head._1.toDouble
    val endPointLongitude = listArr.reverse.head._2.toDouble

    val netDistanceTravelled = Utility.distance(
      startingPointLatitude,
      startingPointLongitude,
      endPointLatitude,
      endPointLongitude,
      "K")
    val netTimeTaken = endTimestamp-startTimestamp
    val speed: Double = netDistanceTravelled/netTimeTaken

    speed
  }

}
