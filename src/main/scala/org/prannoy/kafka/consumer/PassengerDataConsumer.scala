package org.prannoy.kafka.consumer




import java.time.{Duration, Instant}
import java.util
import java.util.Properties
import collection.JavaConverters._
import org.apache.kafka.clients.consumer.{ConsumerRecords, KafkaConsumer}
import org.prannoy.util.{ConfigManager, Utility}

object PassengerDataConsumer {


  Utility.setLogger()

  def main(args: Array[String]): Unit = {
    val passengerConsumer = new KafkaConsumer[String, String](getConsumerProperties())
    passengerConsumer.subscribe(util.Collections.singletonList(ConfigManager.get("kafka.passenger.topic")))

    while(true){
      val passengerRecords: ConsumerRecords[String, String] = passengerConsumer.poll(Duration.ofSeconds(1))
      var recordBuilder: StringBuilder = null
      if (!passengerRecords.isEmpty){
        recordBuilder = StringBuilder.newBuilder
        for (record<-passengerRecords.asScala){
          recordBuilder.append(record.value() + "\n")
        }
        if (recordBuilder != null){
          println(recordBuilder.toString())
          println("\n\n")
          Utility.writeToFile(recordBuilder.toString(), ConfigManager.get("kafka.consumer.write.to.file.path")
            + "/" + Instant.now.getEpochSecond + "-passenger-data.csv"

          )
        }
      }
    }
  }

  def getConsumerProperties(): Properties ={
    val  consumerProperties: Properties = new Properties()
    consumerProperties.put("bootstrap.servers", ConfigManager.get("bootstrap.servers"))
    consumerProperties.put("key.deserializer", ConfigManager.get("key.deserializer"))
    consumerProperties.put("value.deserializer", ConfigManager.get("value.deserializer"))
    consumerProperties.put("group.id", ConfigManager.get("group.id"))
    consumerProperties
  }

}

