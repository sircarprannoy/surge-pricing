package org.prannoy.kafka.consumer

import java.util

import org.apache.kafka.clients.consumer.{ConsumerRecords, KafkaConsumer}
import org.prannoy.kafka.consumer.DriverDataConsumer.getConsumerProperties
import org.prannoy.util.{ConfigManager, Utility}
import java.time.{Duration, Instant}
import collection.JavaConverters._


object WeatherDataConsumer {
  Utility.setLogger()

  def main(args: Array[String]): Unit = {
    val weatherConsumer = new KafkaConsumer[String, String](getConsumerProperties())
    weatherConsumer.subscribe(util.Collections.singletonList(ConfigManager.get("kafka.weather.topic")))

    while(true){
      val weatherRecords: ConsumerRecords[String, String] = weatherConsumer.poll(Duration.ofSeconds(1))
      var recordBuilder: StringBuilder = null
      if (!weatherRecords.isEmpty){
        recordBuilder = StringBuilder.newBuilder
        for (record<-weatherRecords.asScala){
          recordBuilder.append(record.value() + "\n")
        }
        if (recordBuilder != null){
          println(recordBuilder.toString())
          println("\n\n")
          Utility.writeToFile(recordBuilder.toString(), "/Users/prannoy/weather"
          + "/" + Instant.now.getEpochSecond + "-weather-data.csv"

          )
        }
      }
    }
  }

}
