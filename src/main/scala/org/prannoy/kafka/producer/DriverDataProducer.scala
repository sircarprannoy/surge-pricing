package org.prannoy.kafka.producer

import java.time.Instant
import java.util.Properties
import java.util.concurrent.TimeUnit

import ch.hsr.geohash.GeoHash
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import org.prannoy.util.MapedData.latitudeLongitudeMap
import org.prannoy.util.{ConfigManager, MapedData, Utility}

object DriverDataProducer {
  Utility.setLogger()
  def main(args: Array[String]): Unit = {

    val driverTopic = ConfigManager.get("kafka.driver.topic")
    val driverDataproducer: KafkaProducer[Integer, String] = new KafkaProducer[Integer, String](getDriverProducerProperties())
    while(true) {
      val epochTime : Long = Instant.now.getEpochSecond
      pushDriverData(driverDataproducer, driverTopic, epochTime)
      TimeUnit.SECONDS.sleep(10)
    }
  }

  def pushDriverData(producer : KafkaProducer[Integer, String], topic : String, epochTime : Long): Unit ={
    for (driverId <- 1 to 50) {
      val baseLatitude  = latitudeLongitudeMap.get(driverId).get.latitude
      val baseLongitude  = latitudeLongitudeMap.get(driverId).get.longitude
      val newLatitude = baseLatitude + Utility.getRandomNumber(0.002,0.009)
      val newLongitude = baseLongitude + Utility.getRandomNumber(0.002,0.009)
      val geohashString = GeoHash
        .withCharacterPrecision(newLatitude, newLongitude, 6)
        .toBase32
      val recordType = "DRIVER"
      val record = Seq(recordType , driverId, epochTime, newLatitude, newLongitude, geohashString).mkString(",")
      println(record)
      producer.send(new ProducerRecord[Integer, String](topic,  record))
    }
    println("\n\n")
  }

  def getDriverProducerProperties(): Properties ={
    val  producerProperties: Properties = new Properties()
    producerProperties.put("bootstrap.servers", ConfigManager.get("bootstrap.servers"))
    producerProperties.put("key.serializer", ConfigManager.get("key.serializer"))
    producerProperties.put("value.serializer", ConfigManager.get("value.serializer"))
    producerProperties.put("client.id", ConfigManager.get("client.id"))
    producerProperties
  }

}
