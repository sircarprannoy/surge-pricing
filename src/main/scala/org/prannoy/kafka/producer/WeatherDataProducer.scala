package org.prannoy.kafka.producer

import java.time.Instant
import java.util.Properties
import java.util.concurrent.TimeUnit

import ch.hsr.geohash.GeoHash
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import org.joda.time.DateTime
import org.prannoy.kafka.producer.DriverDataProducer.{getDriverProducerProperties, pushDriverData}
import org.prannoy.util.MapedData.latitudeLongitudeMap
import org.prannoy.util.{ConfigManager, MapedData, Utility}

object WeatherDataProducer {
  Utility.setLogger()

  def main(args: Array[String]): Unit = {

    val weatherTopic = ConfigManager.get("kafka.weather.topic")
    val weatherDataproducer: KafkaProducer[Integer, String] = new KafkaProducer[Integer, String](getDriverProducerProperties())
    while(true) {
      val epochTime : Long = Instant.now.getEpochSecond
      pushWeatherData(weatherDataproducer, weatherTopic, epochTime)
      TimeUnit.SECONDS.sleep(5)
    }

  }

  def getProducerProperties(): Properties ={
    val  producerProperties: Properties = new Properties()
    producerProperties.put("bootstrap.servers", ConfigManager.get("bootstrap.servers"))
    producerProperties.put("key.serializer", ConfigManager.get("key.serializer"))
    producerProperties.put("value.serializer", ConfigManager.get("value.serializer"))
    producerProperties.put("client.id", ConfigManager.get("client.id"))
    producerProperties
  }

  def pushWeatherData(producer : KafkaProducer[Integer, String], topic : String, epochTime : Long): Unit ={
      val latitude = latitudeLongitudeMap.get(Utility.getRandomNumber(1,50)).get.latitude
      val longitude = latitudeLongitudeMap.get(Utility.getRandomNumber(1,50)).get.longitude
      val geohashString = GeoHash
        .withCharacterPrecision(latitude, longitude, 3)
        .toBase32
      val  currentHour = new DateTime().getHourOfDay
      val weatherData = MapedData.weatherDataMap.get(currentHour.toString).get
      val recordType = "WEATHER"
      val record = Seq(recordType , epochTime, latitude, longitude, geohashString, weatherData).mkString(",")
//      println(record)
      producer.send(new ProducerRecord[Integer, String](topic,  record))

    println("\n\n")
  }


}
