package org.prannoy.kafka.producer

import java.time.Instant
import java.util.Properties
import java.util.concurrent.TimeUnit

import ch.hsr.geohash.GeoHash
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import org.prannoy.util.MapedData.latitudeLongitudeMap
import org.prannoy.util.{ConfigManager, MapedData, Utility}

object PassengerDataProducer {
  Utility.setLogger()

  def main(args: Array[String]): Unit = {

    val passengerTopic = ConfigManager.get("kafka.passenger.topic")
    val producer: KafkaProducer[Integer, String] = new KafkaProducer[Integer, String](getProducerProperties())
    while(true) {
      val epochTime : Long = Instant.now.getEpochSecond
      pushPassengerData(producer, passengerTopic, epochTime)
      TimeUnit.SECONDS.sleep(Utility.getRandomNumber(0,5))
    }


  }

  def getProducerProperties(): Properties ={
    val  producerProperties: Properties = new Properties()
    producerProperties.put("bootstrap.servers", ConfigManager.get("bootstrap.servers"))
    producerProperties.put("key.serializer", ConfigManager.get("key.serializer"))
    producerProperties.put("value.serializer", ConfigManager.get("value.serializer"))
    producerProperties.put("client.id", ConfigManager.get("client.id"))
    producerProperties
  }

  def pushPassengerData(producer : KafkaProducer[Integer, String], topic : String, epochTime : Long): Unit ={
    val dummyUserTraffic: Int = Utility.getRandomNumber(0,50)
    for (i <- 0 to dummyUserTraffic) {
      val key = Utility.getRandomNumber(1, 100)
      val latitude  = latitudeLongitudeMap.get(key).get.latitude
      val longitude  = latitudeLongitudeMap.get(key).get.longitude
      val geohashString = GeoHash
        .withCharacterPrecision(latitude, longitude, 6)
        .toBase32
      val recordType = "PASSENGER"
      val record = Seq(recordType , key  , epochTime, latitude, longitude, geohashString).mkString(",")
      println(record)
      producer.send(new ProducerRecord[Integer, String](topic,  record))
    }
    println("\n\n")
  }

}
