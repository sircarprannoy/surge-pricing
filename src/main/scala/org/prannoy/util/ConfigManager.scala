package org.prannoy.util

import com.typesafe.config.{Config, ConfigFactory}

object ConfigManager {
  val config : Config = ConfigFactory.load()

  def get(key : String) = {
//    println("Getting conf for key : " + key + " " + config.getString(key))
    config.getString(key)
  }
}
